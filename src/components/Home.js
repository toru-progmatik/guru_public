import React, { useState } from "react";

import UserService from "../services/user.service";
import {Redirect} from "react-router-dom";
import {useSelector} from "react-redux";

const Home = () => {

  const [content, setContent] = useState("");

  const { user: currentUser } = useSelector((state) => state.auth);

  if (!currentUser) {
    return <Redirect to="/login" />;
  }

  return (
    <div className="container">
      <header className="jumbotron">
        <h3>{content}</h3>
      </header>
    </div>
  );
};

export default Home;