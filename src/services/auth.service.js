import axios from "axios";

/*const API_URL = "http://localhost:8080/api/auth/";*/
const API_URL = "http://172.16.0.253:92/frontend/user/";

const register = (username, email, password) => {
  const formData = new FormData();
  formData.append('login', username);
  formData.append('password', password);
  formData.append('email', email);
  return axios.post(API_URL + "register", formData);
};

const forgot = (email) => {
  const formData = new FormData();
  formData.append('email', email);
  return axios.post(API_URL + "forgot", formData);
};

const headers = {
  "Content-Type": "multipart/form-data",
}

const login = (username, password) => {
  const formData = new FormData();
  formData.append('login', username);
  formData.append('password', password);
  return axios
    .post(API_URL + "login", formData, {
      headers: headers
    })
    .then((response) => {
      console.log(response);
      if (response.status == 200 && response.data.token) {
        localStorage.setItem("user", JSON.stringify(response.data.token));
      }
      return response;
    });
};

const logout = () => {
  localStorage.removeItem("user");
};

export default {
  register,
  forgot,
  login,
  logout,
};


//Authorization: Bearer "token"